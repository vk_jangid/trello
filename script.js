let key = '0b976e865cae745a12972bf3d0768956';
let token = '8cbf78caedfda0f7e1c5d804bbe37fbf1d00ff33b028911f0044dec14bbcf6ba';
let trelloContent = document.querySelector('.trello-content');
let count = 0;

function deleteCard(e) {
    console.log(e.target.id);
    fetch(`https://api.trello.com/1/cards/${e.target.parentElement.id}?key=${key}&token=${token}`, {method: 'DELETE'})
        .then(() => {
            window.location.reload();
        });
}

function sendingCardName(e) {
    fetch(`https://api.trello.com/1/cards?idList=5e85881493b5e94ca79e76d7&key=${key}&token=${token}&name=${e.srcElement[0].value}`,{
        method: "POST",
    });
}

function closingAddCardContainer() {
    let addingCardContainer = document.getElementsByClassName('adding-card-container');
    addingCardContainer[0].style.display = "none";
    let addCard = document.getElementsByClassName('add-card');
    addCard[0].style.display = 'block';
}

function addingCard(e) {
    if (count===0) {
        let addCard = document.getElementsByClassName('add-card');
        addCard[0].style.display = 'none';
        let addingCardContainer = document.createElement('form');
        addingCardContainer.style.marginTop = '1em';
        addingCardContainer.className = 'adding-card-container';
        let textBox = document.createElement('input');
        textBox.type = 'text';
        textBox.style.width = '100%';
        textBox.style.marginBottom = '1em';
        textBox.style.borderRadius = '5px';
        let addBtn = document.createElement('button');
        addBtn.className = 'btn btn-success';
        addBtn.textContent = 'Add Card';
        addingCardContainer.addEventListener('submit', sendingCardName);
        let cancelBtn = document.createElement('button');
        cancelBtn.className = 'btn';
        cancelBtn.innerHTML = "<img src='./images/delete-icon.png' alt = ''>";
        cancelBtn.style.marginLeft = '0.5em';
        cancelBtn.addEventListener('click', closingAddCardContainer);
        addingCardContainer.append(textBox, addBtn, cancelBtn);
        trelloContent.firstElementChild.append(addingCardContainer);
        count++;
    } else {
        let addingCardContainer = document.getElementsByClassName('adding-card-container');
        addingCardContainer[0].style.display = "block";
        let addCard = document.getElementsByClassName('add-card');
        addCard[0].style.display = 'none';
    }
    
}

fetch(`https://api.trello.com/1/boards/5e8587f4b197a74fd3c5dde6/lists?key=${key}&token=${token}`)
    .then((res)=> {
        return res.json();
    })
    .then((res)=>{
        trelloContent.firstElementChild.firstElementChild.textContent = res[0].name;
        return res[0].id;
    })
    .then((listId) => {
        fetch(`https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`)
            .then((res) => res.json())
            .then((res) => {
                let cards = Array.from(res);
                cards.forEach((cardObject) => {
                    let card = document.createElement('button');
                    card.className = 'btn btn-light cards';
                    let cardName = document.createElement('span');
                    let deleteButton = document.createElement('button');
                    deleteButton.addEventListener('click', deleteCard);
                    deleteButton.className = 'btn bg-light delete-button';
                    deleteButton.innerHTML = "<img src='./images/delete-icon.png' alt = ''>";
                    deleteButton.id = cardObject.id;
                    deleteButton.firstElementChild.id = cardObject.id;
                    card.append(cardName, deleteButton);
                    cardName.textContent = cardObject.name;
                    trelloContent.firstElementChild.append(card);
                })
                let addCard = document.createElement('button');
                addCard.addEventListener('click', addingCard);
                addCard.className = 'btn btn-light add-card';
                let span = document.createElement('span');
                span.textContent = '+';
                span.style.width = '20px';
                span.style.fontSize = '20px';
                let span2 = document.createElement('span');
                span2.textContent = '  Add Another Card';
                addCard.append(span, span2);
                trelloContent.firstElementChild.append(addCard);
            });
    })
